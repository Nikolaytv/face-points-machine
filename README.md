## Dependencies
python >= 3.4
$ yum install tkinter-python (как-то так)
$ pip install Pillow

## Launching
python key_point_one_test.py full/path/to/the/image.jpg

python key_point_test.py full/path/to/folder/with/images

## Usage
Use numbers from 1 to 7 to add points.

Use N on keyboard to go for the next image.

It creates 2 new folders for images with labels and for point coordinates in the same folder where folder with images is located. 