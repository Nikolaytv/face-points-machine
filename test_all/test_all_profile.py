from scipy.spatial import distance as dist
import argparse
import cv2
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import os
import pandas as pd
import json 

def load(path):

    with open(path) as f:
    head = rows.index('{') + 1
    tail = rows.index('}')

    """Select the point data split into coordinates"""
    raw_points = rows[head:tail]
    coords_set = [point.split() for point in raw_points]

    """Convert entries from lists of strings to tuples of floats"""
    points = [tuple([round(float(point)) for point in coords]) for coords in coords_set]
    return points


def make_point(pts_shape):

    center_point = ((pts_shape[10][0] + pts_shape[11][0]) // 2, (pts_shape[10][1] + pts_shape[11][1]) // 2)
    
    c = dist.euclidean(pts_shape[7], pts_shape[8])
    b = np.sin(np.deg2rad(true_angle)) * c
    a = np.cos(np.deg2rad(true_angle)) * c
    point = [0, 0]
    point[0] = pts_shape[7][0] + int(a)
    point[1] = pts_shape[8][1] + int(b)
    point = tuple(point)
    
    return pts_shape[10], pts_shape[11], center_point


#Доминанта губы
def lip_dominance(pts_shape):

    dY = pts_shape[4][1] - pts_shape[10][1]
    dX = pts_shape[4][0] - pts_shape[10][0]
    axis = np.degrees(np.arctan2(dX, dY))

    dY1 = pts_shape[5][1] - pts_shape[6][1]
    dX1 = pts_shape[5][0] - pts_shape[6][0]
    angle1 = np.degrees(np.arctan2(dX1, dY1))

    lip_angle = angle1 - axis
    
    if lip_angle <= -8:
        lip_angle_cat = 'Верх.доминанта'
        lip_angle_value = 42
    elif -8 < lip_angle <= 0:
        lip_angle_cat = 'Нет доминанты'
        lip_angle_value = 44
    elif 0 < lip_angle:
        lip_angle_cat = 'Ниж.доминанта'
        lip_angle_value = 43
        
    lip_angle_key = 11
    return lip_angle, lip_angle_cat, lip_angle_key, lip_angle_value, angle1, axis
   

#Высота носа
def nose_height(pts_shape):

    nose_dist = dist.euclidean(pts_shape[7], pts_shape[9])
    head_height = dist.euclidean(pts_shape[2], pts_shape[12])
    
    noseHeight = nose_dist / head_height * 1000
    
    if noseHeight <= 240:
        noseHeight_cat = 'Низкий'
        noseHeight_value = 63
    elif 240 < noseHeight <= 265:
        noseHeight_cat = 'Средний'
        noseHeight_value = 65
    elif 265 < noseHeight:
        noseHeight_cat = 'Высокий'
        noseHeight_value = 67
        
    noseHeight_key = 17
    return noseHeight, noseHeight_cat, noseHeight_key, noseHeight_value, nose_dist, head_height


#Наклон носа
def nose_angle(pts_shape):

    dY = pts_shape[4][1] - pts_shape[10][1]
    dX = pts_shape[10][0] - pts_shape[4][0]
    axis = np.degrees(np.arctan2(dX, dY))

    dY1 = pts_shape[8][1] - pts_shape[7][1]
    dX1 = pts_shape[8][0] - pts_shape[7][0]
    angle1 = np.degrees(np.arctan2(dY1, dX1))

    noseAngle = axis + angle1 + 90

    if noseAngle <= 77:
        noseAngle_cat = 'Вздернутый'
        noseAngle_value = 78
    elif 77 < noseAngle <= 89:
        noseAngle_cat = 'Средний'
        noseAngle_value = 79
    elif 89 < noseAngle:
        noseAngle_cat = 'Опущенный' 
        noseAngle_value = 80
        
    noseAngle_key = 20
    return noseAngle, noseAngle_cat, noseAngle_key, noseAngle_value, axis, angle1


#Угол челюсти
def jaw_angle(pts_shape):
    
    dY = pts_shape[4][1] - pts_shape[10][1]
    dX = pts_shape[4][0] - pts_shape[10][0]
    axis = np.degrees(np.arctan2(dX, dY))

    dY1_2 = pts_shape[1][1] - pts_shape[0][1]
    dX1_2 = pts_shape[0][0] - pts_shape[1][0]
    angle1_2 = np.degrees(np.arctan2(dX1_2, dY1_2))

    angle1_2 = angle1_2 - axis 
    dY2_3 = pts_shape[1][1] - pts_shape[2][1]
    dX2_3 = pts_shape[1][0] - pts_shape[2][0]
    angle2_3 = np.degrees(np.arctan2(dY2_3, dX2_3))
    angle2_3 = angle2_3 - axis + 180
    
    jawAngle = angle2_3 - angle1_2 + 90

    if jawAngle <= 117:
        jawAngle_cat = 'Прямой' 
        jawAngle_value = 17

    elif 117 < jawAngle <= 125:
        jawAngle_cat = 'Cредне-прямой'
        jawAngle_value = 16
    elif 125 < jawAngle:
        jawAngle_cat = 'Отсутствует'
        jawAngle_value = 14

    jawAngle_key = 4
    return jawAngle, jawAngle_cat, jawAngle_key, jawAngle_value, angle1_2, angle2_3
 

#Профиль
def profile(pts_shape):
    
    dY = pts_shape[2][1] - pts_shape[6][1]
    dX = pts_shape[6][0] - pts_shape[2][0]
    angle = np.degrees(np.arctan2(dX, dY))
    
    dY = pts_shape[10][1] - pts_shape[12][1]
    dX = pts_shape[10][0] - pts_shape[12][0]
    angle2 = np.degrees(np.arctan2(dX, dY))

    proFile = angle + angle2
    
    if proFile <= 33:
        proFile_cat = 'Рац' 
        proFile_value = 239

    elif 33 < proFile <= 41:
        proFile_cat = 'Средний'
        proFile_value = 241
    elif 41 < proFile:
        proFile_cat = 'Иррац'
        proFile_value = 240
    
    
    proFile_key = 72
    return proFile, proFile_cat, proFile_key, proFile_value, angle, angle2

    
#Скос лба
def skos_lba(pts_shape):
    
    dY = pts_shape[4][1] - pts_shape[10][1]
    dX = pts_shape[4][0] - pts_shape[10][0]
    axis = np.degrees(np.arctan2(dY, dX)) - 90
        
    dY2 = pts_shape[10][1] - pts_shape[12][1]
    dX2 = pts_shape[10][0] - pts_shape[12][0]
    angle2 = np.degrees(np.arctan2(dY2, dX2)) - 90
    
    skosLba =  axis - angle2
    
    if skosLba <= 3:
        skosLba_cat = 'Выпуклый'
        skosLba_value = 134
    if skosLba <= 10:
        skosLba_cat = 'Прямой'
        skosLba_value = 133
    elif 10 < skosLba <= 15:
        skosLba_cat = 'Средне-прямой'
        skosLba_value = 132
    elif 14 < skosLba <= 22:
        skosLba_cat = 'Средне-скошенный'
        skosLba_value = 131
    elif 22 < skosLba:
        skosLba_cat = 'Скошенный' 
        skosLba_value = 130

    skosLba_key = 72
    return skosLba, skosLba_cat, skosLba_key, skosLba_value, axis, angle2


#Выступ подбородка
def chin_ledge(pts_shape):

    dY = pts_shape[3][1] - pts_shape[10][1]
    dX = pts_shape[3][0] - pts_shape[10][0]
    axis = np.degrees(np.arctan2(dY, dX)) - 90
        
    dY = pts_shape[3][1] - pts_shape[4][1]
    dX = pts_shape[3][0] - pts_shape[4][0]
    angle = np.degrees(np.arctan2(dY, dX)) - 90

    chinLedge = axis - angle
   
    if chinLedge <= 2:
        chinLedge_cat = 'Срезанный'
        chinLedge_value = 20
    elif 2 < chinLedge <= 8:
        chinLedge_cat = 'Средне-срезанный'
        chinLedge_value = 21
    elif 8 < chinLedge <= 12:
        chinLedge_cat = 'Средне-выступающий'
        chinLedge_value = 22
    elif 12 < chinLedge:
        chinLedge_cat = 'Выступающий' 
        chinLedge_value = 24
        
    chinLedge_key = 6    
    return chinLedge, chinLedge_cat, chinLedge_key, chinLedge_value, axis, angle


#Тяжесть челюсти
def jaw_weight(pts_shape):

    dY = pts_shape[4][1] - pts_shape[10][1]
    dX = pts_shape[4][0] - pts_shape[10][0]
    axis = np.degrees(np.arctan2(dX, dY))

    dY = pts_shape[1][1] - pts_shape[0][1]
    dX = pts_shape[1][0] - pts_shape[0][0]
    angle = np.degrees(np.arctan2(dX, dY))
    
    true_angle = angle - axis

        #Make point
    c = dist.euclidean(pts_shape[0], pts_shape[1])
    b = np.sin(np.deg2rad(true_angle)) * c
    a = np.cos(np.deg2rad(true_angle)) * c
    point = [0, 0]
    point[0] = pts_shape[1][0] - int(b)
    point[1] = pts_shape[0][1] + int(a)
    point = tuple(point)

    jawWeight = dist.euclidean(pts_shape[0], point) +  dist.euclidean(pts_shape[2], pts_shape[4])

    head_high = dist.euclidean(pts_shape[7], pts_shape[12])
    
    jawWeight = jawWeight / head_high * 10
   
    if jawWeight <= 6.8:
        jawWeight_cat = 'Легкая'
        jawWeight_value = 6
    elif 6.8 < jawWeight <= 7.8:
        jawWeight_cat = 'Средняя'
        jawWeight_value = 8
    elif 7.8 < jawWeight:
        jawWeight_cat = 'Тяжелая' 
        jawWeight_value = 10
        
    jawWeight_key = 2    
    return jawWeight, jawWeight_cat, jawWeight_key, jawWeight_value, jawWeight, point


#Глубина глаз
def eye_deepness(pts_shape):

    dY = pts_shape[4][1] - pts_shape[10][1]
    dX = pts_shape[10][0] - pts_shape[4][0]
    axis = np.degrees(np.arctan2(dX, dY))

    dY = pts_shape[13][1] - pts_shape[10][1]
    dX = pts_shape[13][0] - pts_shape[10][0]  
    angle1 = np.degrees(np.arctan2(dY, dX))
    angle1 = 180 - angle1 + axis
    
            #Make point
    c = dist.euclidean(pts_shape[13], pts_shape[10])
    b = np.sin(np.deg2rad(angle1)) * c
    a = np.cos(np.deg2rad(angle1)) * c
    point = [0, 0]
    point[0] = pts_shape[13][0] + int(a)
    point[1] = pts_shape[10][1] + int(b)
    point = tuple(point)

    eye_deep_dist = dist.euclidean(pts_shape[13], point)
    head_width = dist.euclidean(pts_shape[0], pts_shape[7])
#     eyeDepness = axis - angle
    eyeDepness = eye_deep_dist / head_width * 100
    
    if eyeDepness <= 18:
        eyeDepness_cat = 'Выпуклые'
        eyeDepness_value = 105
    elif 18 < eyeDepness <= 20:
        eyeDepness_cat = 'Средне'
        eyeDepness_value = 104
    elif 20 < eyeDepness:
        eyeDepness_cat = 'Глубокие' 
        eyeDepness_value = 103
    eyeDepness_key = 28    

    return eyeDepness, eyeDepness_cat, eyeDepness_key, eyeDepness_value, axis, angle1, point

#Выступ челюсти
def jaw_ledge(pts_shape):

    dY = pts_shape[3][1] - pts_shape[10][1]
    dX = pts_shape[10][0] - pts_shape[3][0]
    axis = np.degrees(np.arctan2(dY, dX))
        
    dY = pts_shape[1][1] - pts_shape[0][1]
    dX = pts_shape[0][0] - pts_shape[1][0]
    angle = np.degrees(np.arctan2(dY, dX))

    jawLedge = angle - axis
   
    if jawLedge <= 6:
        jaw_ledge_cat = 'Втянутая'
        jaw_ledge_value = 11
    elif 6 < jawLedge <= 10:
        jaw_ledge_cat = 'Средняя'
        jaw_ledge_value = 12
    elif 10 < jawLedge:
        jaw_ledge_cat = 'Выступающая' 
        jaw_ledge_value = 13
    jaw_ledge_key = 3 

    return jawLedge, jaw_ledge_cat, jaw_ledge_key, jaw_ledge_value, axis, angle


#Длина носа
def nose_length(pts_shape):
    
    #Make point of axis on the level of nose
    dX = pts_shape[8][0] - pts_shape[4][0]
    dY = pts_shape[4][1] - pts_shape[8][1]
    angle = np.degrees(np.arctan2(dX, dY)) 
    
    dX = pts_shape[10][0] - pts_shape[4][0]
    dY = pts_shape[4][1] - pts_shape[10][1]
    axis = np.degrees(np.arctan2(dX, dY))
    
    dX = pts_shape[9][0] - pts_shape[4][0]
    dY = pts_shape[4][1] - pts_shape[9][1]
    axis2 = np.degrees(np.arctan2(dX, dY))

    axis = (axis + axis2) / 2
    
    true_angle = angle - axis
#     noseLength = nose_length / head_height * 1000

        #Make point
    c = dist.euclidean(pts_shape[4], pts_shape[8])
    b = np.sin(np.deg2rad(true_angle)) * c
    a = np.cos(np.deg2rad(true_angle)) * c
    point2 = [0, 0]
    point2[0] = pts_shape[8][0] - int(b)
    point2[1] = pts_shape[4][1] - int(a)
    point2 = tuple(point2)

    head_height = dist.euclidean(pts_shape[2], pts_shape[12])
    head_weight = dist.euclidean(pts_shape[0], pts_shape[7])
    
    nose_length = dist.euclidean(point2, pts_shape[8])
    noseLength = nose_length / (head_height + head_weight) * 1000
    
    if noseLength <= 79:
        noseLength_cat = 'Короткий'
        noseLength_value = 73
    elif 79 < noseLength <= 85:
        noseLength_cat = 'Средний'
        noseLength_value = 75
    elif 85 < noseLength:
        noseLength_cat = 'Длинный'
        noseLength_value = 77
        
    noseLength_key = 19
    return noseLength, noseLength_cat, noseLength_key, noseLength_value, head_height, point2


#Впалость переносицы
def nose_bridge(pts_shape):

    dY = pts_shape[8][1] - pts_shape[9][1]
    dX = pts_shape[8][0] - pts_shape[9][0]
    axis1 = np.degrees(np.arctan2(dX, dY))
    
    dY = pts_shape[8][1] - pts_shape[12][1]
    dX = pts_shape[8][0] - pts_shape[12][0]
    axis2 = np.degrees(np.arctan2(dX, dY))
    
    axis = axis1 - axis2 

    noseBridge = axis
    if noseBridge <= 10:
        noseBridge_cat = 'Прямая' 
        noseBridge_value = 91
    elif 10 < noseBridge <= 14:
        noseBridge_cat = 'Средняя'
        noseBridge_value = 173
    elif 14 < noseBridge:
        noseBridge_cat = 'Впалая'
        noseBridge_value = 92
        
    noseBridge_key = 24
    return noseBridge, noseBridge_cat, noseBridge_key, noseBridge_value, axis1, axis2



#Надбровные дуги
def brow_ridges(pts_shape):

    dY = pts_shape[10][1] - pts_shape[11][1]
    dX = pts_shape[10][0] - pts_shape[11][0]
    angle1 = np.degrees(np.arctan2(dY, dX))
    
    dY = pts_shape[10][1] - pts_shape[12][1]
    dX = pts_shape[10][0] - pts_shape[12][0]
    angle2 = np.degrees(np.arctan2(dY, dX))
    
    browRidge = angle1 - angle2

    if browRidge <= 12:
        browRidge_cat = 'Не выражены'
        browRidge_value = 127
    elif 12 < browRidge <= 18:
        browRidge_cat = 'Средне-выражены'
        browRidge_value = 128
    elif 18 < browRidge:
        browRidge_cat = 'Выражены' 
        browRidge_value = 129
    browRidge_key = 36
    
    return browRidge, browRidge_cat, browRidge_key, browRidge_value, angle1, angle2

#Зоны лица
def zone(pts_shape):

        #TOP ZONE
    dY = pts_shape[10][1] - pts_shape[12][1]
    dX = pts_shape[12][0] - pts_shape[10][0]
    axis1 = np.degrees(np.arctan2(dX, dY))
    
    dY = pts_shape[10][1] - pts_shape[13][1]
    dX = pts_shape[13][0] - pts_shape[10][0]
    axis2 = np.degrees(np.arctan2(dX, dY))
    
    lobVypuk = axis1 - axis2 

        #Upper
    dY = pts_shape[4][1] - pts_shape[10][1]
    dX = pts_shape[10][0] - pts_shape[4][0]
    axis = np.degrees(np.arctan2(dX, dY))

    angle = axis - axis2 
    
    if lobVypuk <= 10:
        #Make point
        c = dist.euclidean(pts_shape[10], pts_shape[12])
        b = np.sin(np.deg2rad(angle)) * c
        a = np.cos(np.deg2rad(angle)) * c
        point = [0, 0]
        point[0] = pts_shape[13][0] + int(b)
        point[1] = pts_shape[10][1] - int(a)
        pointTop = tuple(point)
        topDist = dist.euclidean(pts_shape[10], pointTop)
        center_point = pts_shape[12]
    
    elif lobVypuk >= 10:
        center_point = ((pts_shape[12][0] + pts_shape[13][0]) // 2, (pts_shape[12][1] + pts_shape[13][1]) // 2)
   
        #Make point
        c = dist.euclidean(pts_shape[10], center_point)
        b = np.sin(np.deg2rad(angle)) * c
        a = np.cos(np.deg2rad(angle)) * c
        point = [0, 0]
        point[0] = pts_shape[12][0] + int(b)
        point[1] = pts_shape[10][1] - int(a)
        pointTop = tuple(point)
        topDist = dist.euclidean(pts_shape[10], pointTop)
    
        #MID ZONE
    midDist = dist.euclidean(pts_shape[7], pts_shape[10])
    
        #BOT ZONE    
#     botDist = (dist.euclidean(pts_shape[2], pts_shape[7]) + dist.euclidean(pts_shape[1], pts_shape[0])) / 2
    botDist = dist.euclidean(pts_shape[2], pts_shape[7])
    
        #CALCULATING DOMINANT ZONE
    average = (topDist + midDist + botDist) / 3 

    if topDist > average and midDist > average:
        mainZone_cat = 'Верхняя и Средняя'
        
    elif topDist > average and botDist > average:
        mainZone_cat = 'Верхняя и Нижняя'
    
    elif midDist > average and botDist > average:
        mainZone_cat = 'Средняя и Нижняя'
    
    elif topDist > average:
        mainZone_cat = 'Верхняя'
    
    elif botDist > average:
        mainZone_cat = 'Нижняя'
    
    elif midDist > average:
        mainZone_cat = 'Средняя'
    
    return round(lobVypuk), mainZone_cat, round(topDist), round(midDist), round(botDist), round(average), center_point, pointTop


#Выпуклость глаз
def eye_bubble(pts_shape):
    pts_shape 

    eye_width = dist.euclidean(pts_shape[14], pts_shape[15])
    head_width = dist.euclidean(pts_shape[0], pts_shape[7])
    eyeBubble = head_width / eye_width 
    
    if eyeBubble <= 7.3:
        eyeBubble_cat = 'Впалые'
        eyeBubble_value = 127
    elif 7.3 < eyeBubble <= 8.0:
        eyeBubble_cat = 'Средне-впалые'
        eyeBubble_value = 128
    elif 8.0 < eyeBubble <= 9.5:
        eyeBubble_cat = 'Средне-выпуклые'
        eyeBubble_value = 128
    elif 9.5 < eyeBubble:
        eyeBubble_cat = 'Выпуклые' 
        eyeBubble_value = 129
    eyeBubble_key = 46
    
    return eyeBubble, eyeBubble_cat, eyeBubble_key, eyeBubble_value, eye_width, head_width


def print_func(pts_shape):

    lip_angle, lip_angle_cat, lip_angle_key, lip_angle_value, angle1, axis = lip_dominance(pts_shape)
    print('Доминанта губы    ', lip_angle_cat, lip_angle)

    noseAngle, noseAngle_cat, noseAngle_key, noseAngle_value, axis, angle1= nose_angle(pts_shape)
    print('Наклон носа       ', noseAngle_cat ,noseAngle)

    noseLength, noseLength_cat, noseLength_key, noseLength_value, point, true_angle = nose_length(pts_shape)
    print('Длина носа        ', noseLength_cat, noseLength)

    noseBridge, noseBridge_cat, noseBridge_key, noseBridge_value, axis, angle2 = nose_bridge(pts_shape)
    print('Переносица        ', noseBridge_cat, noseBridge)

    eyeBubble, eyeBubble_cat, eyeBubble_key, eyeBubble_value, eye_width, head_width = eye_bubble(pts_shape)
    print('Выпуклость глаз      ', eyeBubble_cat, eyeBubble)

    proFile, proFile_cat, proFile_key, proFile_value, angle, angle2 = profile(pts_shape)
    print('Профиль лица      ', proFile_cat, proFile)

    skosLba, skosLba_cat, skosLba_key, skosLba_value, axis, angle2 = skos_lba(pts_shape)
    print('Скос лба          ', skosLba_cat, skosLba)

    browRidge, browRidge_cat, browRidge_key, browRidge_value, axis, angle = brow_ridges(pts_shape)
    print('Надбровные дуги   ', browRidge_cat, browRidge)

    chinLedge, chinLedge_cat, chinLedge_key, chinLedge_value, axis, angle = chin_ledge(pts_shape)
    print('Выступ подбородка ', chinLedge_cat, chinLedge)

    jawWeight, jawWeight_cat, jawWeight_key, jawWeight_value, jaw_profile_perimetr, head_profile_perimetr = jaw_weight(pts_shape)
    print('Тяжесть челюсти   ', jawWeight_cat, jawWeight)

    jawAngle, jaw_angle_cat, jaw_angle_key, jaw_angle_value, axis, angle = jaw_angle(pts_shape)
    print('Угол челюсти      ', jaw_angle_cat, jawAngle)

    jawLedge, jaw_ledge_cat, jaw_ledge_key, jaw_ledge_value, axis, angle = jaw_ledge(pts_shape)
    print('Выступ челюсти    ', jaw_ledge_cat, jawLedge)

    lobVypuk, mainZone_cat, topDist, midDist, botDist, average, center_point, pointTop = zone(pts_shape)
    print('Дом. зона лица    ', mainZone_cat, lobVypuk, topDist, midDist, botDist, average)

    if 10 < lip_angle or lip_angle < -40:
        print('ETO GG - губы')
    if 120 < noseAngle or noseAngle < 30:
        print('ETO GG - угол носа')
    if 130 < noseLength or noseLength < 15:
        print('ETO GG - длина носа')
    if 40 < noseBridge  or noseBridge < -5:
        print('ETO GG - переносица')
    if 40 < eyeBubble or eyeBubble < 0.2:
        print('ETO GG - выпуклость глаз')
    if 80 < proFile or proFile < 5:
        print('ETO GG - профиль')
    if 45 < skosLba or skosLba < -5:
        print('ETO GG - скос лба')
    if 35 < chinLedge or chinLedge < -20:
        print('ETO GG - выступ подбородка')
    if 30 < jawWeight or jawWeight < 0.5:
        print('ETO GG - тяжесть челюсти')
    if 180 < jawAngle or jawAngle < 80:
        print('ETO GG - угол челюсти')
    if 60 < jawLedge or jawLedge < -20:
        print('ETO GG - выступ челюсти')


def visualize(image, pts_shape):
    for (x, y) in pts_shape:
        cv2.circle(image, (x, y), 2, (0, 0, 255), -1)
        cv2.namedWindow('window', cv2.WINDOW_AUTOSIZE)
    cv2.imshow('window', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows() 


def main():
    count = 0

    path = 'gold__profile_dataset/'
    for img in os.listdir(path):
        if img.endswith('jpg'):
            print('****************************')
            print('Number {}'.format(count))
            print(img[:-12])
            image_path = '{}/{}'.format(path, img)
            pts_shape_path = '{}/{}.pts'.format(path, img[:-12])

            pts_shape = load(pts_shape_path)
            image = cv2.imread(image_path)

            print_func(pts_shape)
            visualize(image, pts_shape)
            count += 1
if __name__ == "__main__":
    main()




























