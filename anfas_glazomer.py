from scipy.spatial import distance as dist
import argparse
import cv2
import os
import json 
import dlib
import numpy as np


def load(path):
    with open(path) as f:
        rows = [rows.strip() for rows in f]
    
    head = rows.index('{') + 1
    tail = rows.index('}')

    raw_points = rows[head:tail]
    coords_set = [point.split() for point in raw_points]

    points = [tuple([round(float(point)) for point in coords]) for coords in coords_set]
    return points


def shape_to_np(shape, dtype="int"):
    # initialize the list of (x, y)-coordinates
    coords = np.zeros((68, 2), dtype=dtype)
 
    # loop over the 68 facial landmarks and convert them
    # to a 2-tuple of (x, y)-coordinates
    for i in range(0, 68):
        coords[i] = (shape.part(i).x, shape.part(i).y)
    
    # return the list of (x, y)-coordinates
    return coords

#Форма головы
def head_form(pts_shape):
    
    head_height = dist.euclidean(pts_shape[14], pts_shape[0])
    head_width = dist.euclidean(pts_shape[7], pts_shape[8])
    headForm = head_height / head_width
    
    if headForm <= 1.40:
        head_form_cat = 'Брахи'
        head_form_value = 181
    elif 1.40 < headForm <= 1.45:
        head_form_cat = 'Мезо'
        head_form_value = 182
    elif 1.45 < headForm:
        head_form_cat = 'Долих'
        head_form_value = 183
    
    head_form_key = 51
    return headForm, head_form_cat, head_form_key, head_form_value, head_height, head_width


#Наклон бровей
def brow_angle(pts_shape):
        # extract the left and right eye (x, y)-coordinates
    leftEyePts = shape[42:46]
    rightEyePts = shape[36:40]  
    
        # compute the center of mass for each eye
    leftEyeCenter = leftEyePts.mean(axis=0).astype("int")
    rightEyeCenter = rightEyePts.mean(axis=0).astype("int")

        # compute the angle between the eye centroids
    dY = leftEyeCenter[1] - rightEyeCenter[1]
    dX = rightEyeCenter[0] - leftEyeCenter[0]
    axis = np.degrees(np.arctan2(dX, dY)) + 90
        
        #### Right Brow ####
    dY = pts_shape[1][1] - pts_shape[2][1]
    dX = pts_shape[1][0] - pts_shape[2][0]
    angleRight = np.degrees(np.arctan2(dY, dX))
    
            #### Left Brow ####
    dY = pts_shape[3][1] - pts_shape[4][1]
    dX = pts_shape[4][0] - pts_shape[3][0]
    angleLeft = np.degrees(np.arctan2(dY, dX))
    
    browAngle = axis*2 + (angleRight + angleLeft) / 2
    
    if browAngle <= 14:
        browAngle_cat = 'Без наклона'
        browAngle_value = 260
    elif 14 < browAngle <= 22:
        browAngle_cat = 'Средние'
        browAngle_value = 261
    elif 22 < browAngle:
        browAngle_cat = 'Атакующие'
        browAngle_value = 262
    
    browAngle_key = 77
    return browAngle, browAngle_cat, browAngle_key, browAngle_value, angleLeft, angleRight


#Размер глаз
def eye_size(pts_shape): 
    # left and right eye width
    LEwidth = dist.euclidean(shape[42], shape[45])
    REwidth = dist.euclidean(shape[36], shape[39])
    MEwidth = (LEwidth + REwidth)/2

    # left and right eye height
    LEheight = (dist.euclidean(shape[43], shape[47]) + dist.euclidean(shape[44], shape[46]))/2
    REheight = (dist.euclidean(shape[37], shape[41]) + dist.euclidean(shape[38], shape[40]))/2
    MEheight = (LEheight + REheight)/2

    head_width = dist.euclidean(pts_shape[15], pts_shape[8])
    head_height = dist.euclidean(pts_shape[14], pts_shape[0])
    perimetr = dist.euclidean(pts_shape[7], pts_shape[11]) + dist.euclidean(pts_shape[11], pts_shape[13]) + dist.euclidean(pts_shape[13], pts_shape[15]) \
    + dist.euclidean(pts_shape[15], pts_shape[12]) + dist.euclidean(pts_shape[12], pts_shape[8])
    EyeSize = (MEheight + MEwidth) / (perimetr) * 10

# ВЫВЕСТИ РАЗМЕР ГЛАЗ
    if EyeSize <= 1.15:
        EyeSize_cat = 'Маленькие'
        eye_size_value = 95

    elif 1.15 < EyeSize <= 1.21:
        EyeSize_cat =  'Средние'
        eye_size_value = 97

    elif EyeSize > 1.21:
        EyeSize_cat = 'Большие'
        eye_size_value = 99
    
    eye_size_key = 26

    return EyeSize, EyeSize_cat, eye_size_key, eye_size_value, MEheight, MEwidth


#Ширина посадки глаз. Отношение расстояния между глаз к расстоянию от глаз до висков
def eye_width(pts_shape):

    # extract the left and right eye (x, y)-coordinates
    leftEyePts = shape[42:46]
    rightEyePts = shape[36:40]

    # compute the center of mass for each eye
    leftEyeCenter = leftEyePts.mean(axis=0).astype("int")
    rightEyeCenter = rightEyePts.mean(axis=0).astype("int")

    # compute the angle between the eye centroids
    dY1 = rightEyeCenter[1] - leftEyeCenter[1]
    dX1= rightEyeCenter[0] - leftEyeCenter[0]
    axis = np.degrees(np.arctan2(dX1, dY1)) + 90

    #### MAKE POINT RIGHT #####
    dY = rightEyeCenter[1] - pts_shape[7][1]
    dX = rightEyeCenter[0] - pts_shape[7][0]
    angle = np.degrees(np.arctan2(dY, dX))
    
    true_angle = angle - axis
    
        #Make point
    c = dist.euclidean(rightEyeCenter, pts_shape[7])
    b = np.sin(np.deg2rad(true_angle)) * c
    a = np.cos(np.deg2rad(true_angle)) * c
    point = [0, 0]
    point[1] = pts_shape[7][1] + int(b)
    point[0] = rightEyeCenter[0] - int(a)
    pointRight = tuple(point)
    
        #### MAKE POINT LEFT ####
    dY = leftEyeCenter[1] - pts_shape[8][1]
    dX = pts_shape[8][0] - leftEyeCenter[0] 
    angle = np.degrees(np.arctan2(dY, dX))
    
    true_angle = angle + axis
    
        #Make point
    c = dist.euclidean(leftEyeCenter, pts_shape[8])
    b = np.sin(np.deg2rad(true_angle)) * c
    a = np.cos(np.deg2rad(true_angle)) * c
    point2 = [0, 0]
    point2[1] = pts_shape[8][1] + int(b)
    point2[0] = leftEyeCenter[0] + int(a)
    pointLeft = tuple(point2)
    
    betweenEyeWidth = dist.euclidean(shape[39], shape[42])
    templeEye = dist.euclidean(shape[36], pointRight) + dist.euclidean(shape[45], pointLeft)
    
    eyeWidth = betweenEyeWidth / templeEye
    
    if eyeWidth <= 0.95:
        eyeWidth_cat = 'Близко'
        eyeWidth_value = 100
    elif 0.95 < eyeWidth <= 1.05:
        eyeWidth_cat = 'Средне'
        eyeWidth_value = 101
    elif eyeWidth > 1.05:
        eyeWidth_cat = 'Далеко'
        eyeWidth_value = 102
        
    eyeWidth_key = 27
    
    return eyeWidth, eyeWidth_cat, eyeWidth_key, eyeWidth_value, pointRight, pointLeft


#Ширина переносицы
def bridge_width(pts_shape): 
    
    bridgeDist = dist.euclidean(pts_shape[21], pts_shape[22])
    headWidth = dist.euclidean(pts_shape[7], pts_shape[8])
    bridgeWidth = bridgeDist / headWidth * 100
    
    if bridgeWidth <= 11.5:
        bridgeWidth_cat = 'Узкая'
        bridgeWidth_value = 86

    elif 11.5 < bridgeWidth <= 12.5:
        bridgeWidth_cat = 'Средняя'
        bridgeWidth_value = 88

    elif bridgeWidth > 12.5:
        bridgeWidth_cat = 'Широкая'
        bridgeWidth_value = 90
        
    bridgeWidth_key = 23
    return bridgeWidth, bridgeWidth_cat, bridgeWidth_key, bridgeWidth_value, headWidth, headWidth


#Размер рта
def mouth_size(pts_shape): 
    
    mouthDist =  dist.euclidean(shape[60], shape[64])
    headWidth = dist.euclidean(pts_shape[7], pts_shape[8])

    mouthSize = mouthDist / headWidth
    if mouthSize <= 0.31:
        mouthSize_cat = 'Маленький'
        mouthSize_value = 86

    elif 0.31 < mouthSize <= 0.35:
        mouthSize_cat = 'Средний'
        mouthSize_value = 88

    elif mouthSize > 0.35:
        mouthSize_cat = 'Большой'
        mouthSize_value = 31
        
    mouthSize_key = 8
    return mouthSize, mouthSize_cat, mouthSize_key, mouthSize_value, headWidth, mouthDist


#Ширина челюсти
def jaw_width(pts_shape): 
    
    jawWidth =  dist.euclidean(pts_shape[11], pts_shape[12])
    headWidth = dist.euclidean(pts_shape[7], pts_shape[8])

    jawWidth = jawWidth / headWidth
    if jawWidth <= 0.84:
        jawWidth_cat = 'Узкая'
        jawWidth_value = 1

    elif 0.84 < jawWidth <= 0.90:
        jawWidth_cat = 'Средняя'
        jawWidth_value = 3

    elif jawWidth > 0.90:
        jawWidth_cat = 'Широкая'
        jawWidth_value = 5
        
    jawWidth_key = 1
    return jawWidth, jawWidth_cat, jawWidth_key, jawWidth_value, jawWidth, headWidth


#Ширина подбородка
def chin_width(pts_shape): 
    
    dY = pts_shape[14][1] - pts_shape[15][1]
    dX = pts_shape[15][0] - pts_shape[14][0]
    angle = 90 - np.degrees(np.arctan2(dY, dX))
    
    dY = pts_shape[14][1] - pts_shape[13][1]
    dX = pts_shape[14][0] - pts_shape[13][0]
    angle2 = 90 - np.degrees(np.arctan2(dY, dX))
    
    chinWidth = angle + angle2
    
    if chinWidth <= 145:
        chinWidth_cat = 'Острый'
        chinWidth_value = 214

    elif 145 < chinWidth <= 155:
        chinWidth_cat = 'Средний'
        chinWidth_value = 213

    elif chinWidth > 155:

        chinWidth_cat = 'Широкий'
        chinWidth_value = 212
    chinWidth_key = 59
    return chinWidth, chinWidth_cat, chinWidth_key, chinWidth_value, angle, angle2


#Тяжесть подбородка
def chin_weight(pts_shape):
    
    chinWeight =  dist.euclidean(pts_shape[14], pts_shape[15])
    headHeight = dist.euclidean(pts_shape[14], pts_shape[0])
    headWidth = dist.euclidean(pts_shape[7], pts_shape[8])

    chinWeight = chinWeight / headWidth * 10
    if chinWeight <= 2.25:
        chinWeight_cat = 'Легкий'
        chinWeight_value = 211
    elif 2.25 < chinWeight <= 2.65:
        chinWeight_cat = 'Средний'
        chinWeight_value = 210
    elif chinWeight > 2.65:
        chinWeight_cat = 'Тяжелый'
        chinWeight_value = 209
        
    chinWeight_key = 58
    return chinWeight, chinWeight_cat, chinWeight_key, chinWeight_value, chinWeight, headHeight


#Асимметрия желваков
def jelvak_asymetry(pts_shape):
    
    pixel_between_lips2 = (abs(shape[62][1] - shape[66][1]))/2
    if shape[62][1] > shape[66][1]:
        shape[62][1] = shape[62][1] - pixel_between_lips2
        shape[66][1] =  shape[66][1] + pixel_between_lips2
    elif shape[62][1] < shape[66][1]:
        shape[62][1] = shape[62][1] + pixel_between_lips2
        shape[66][1] =  shape[66][1] - pixel_between_lips2
    
    leftJelvak =  dist.euclidean(pts_shape[12], shape[66])
    rightJelvak =  dist.euclidean(pts_shape[11], shape[66])
    jelvakAsymm = leftJelvak / rightJelvak
    
    if jelvakAsymm <= 0.97:
        jelvakAsymm_cat = 'Правый'
        jelvakAsymm_value = 184
    elif 0.97 < jelvakAsymm <= 1.03:
        jelvakAsymm_cat = 'Нет асимметрии'
        jelvakAsymm_value = 19
    elif jelvakAsymm > 1.03:
        jelvakAsymm_cat = 'Левый'
        jelvakAsymm_value = 18
        
    jelvakAsymm_key = 58
    return jelvakAsymm, jelvakAsymm_cat, jelvakAsymm_key, jelvakAsymm_value, leftJelvak, rightJelvak

#Ширина ноздрей
def nostrils_width(pts_shape):
    
    nostrilsWidth =  dist.euclidean(pts_shape[17], pts_shape[18])
    headWidth = dist.euclidean(pts_shape[7], pts_shape[8])

    nostrilsWidth = nostrilsWidth / headWidth * 10
    if nostrilsWidth <= 2.55:
        nostrilsWidth_cat = 'Узкие'
        nostrilsWidth_value = 81
    elif 2.55 < nostrilsWidth <= 2.8:
        nostrilsWidth_cat = 'Средние'
        nostrilsWidth_value = 82
    elif nostrilsWidth > 2.8:
        nostrilsWidth_cat = 'Широкие'
        nostrilsWidth_value = 83 
        
    nostrilsWidth_key = 58
    return nostrilsWidth, nostrilsWidth_cat, nostrilsWidth_key, nostrilsWidth_value, nostrilsWidth, nostrilsWidth


#Асимметрия челюсти
def jaw_asymmetry(pts_shape):
    dY = shape[66][1] - shape[27][1]
    dX = shape[66][0] - shape[27][0]
    axis = np.degrees(np.arctan2(dX, dY))

    dY = pts_shape[14][1] - shape[66][1]
    dX = pts_shape[14][0] - shape[66][0]
    angle = np.degrees(np.arctan2(dY, dX)) - 90
    
    jawAsymm = angle - axis
    if jawAsymm <= -0.5:
        jawAsymm_cat = 'Влево'
        jawAsymm_value = 25
    elif -0.5 < jawAsymm <= 0.5:
        jawAsymm_cat = 'Нет асимметрии'
        jawAsymm_value = 244
    elif jawAsymm > 0.5:
        jawAsymm_cat = 'Вправо'
        jawAsymm_value = 266 
        
    jawAsymm_key = 7
    return jawAsymm, jawAsymm_cat, jawAsymm_key, jawAsymm_value, axis, angle


#Асимметрия подбородка
def chin_asymmetry(pts_shape):
    
    dY = shape[66][1] - shape[27][1]
    dX = shape[66][0] - shape[27][0]
    axis = np.degrees(np.arctan2(dX, dY))

    dY = pts_shape[13][1] - pts_shape[15][1]
    dX = pts_shape[15][0] - pts_shape[13][0]
    angle = np.degrees(np.arctan2(dY, dX))
    
    chinAsymm = angle - axis
    if chinAsymm <= -0.5:
        chinAsymm_cat = 'Влево'
        chinAsymm_value = 25
    elif -0.5 < chinAsymm <= 0.5:
        chinAsymm_cat = 'Нет асимметрии'
        chinAsymm_value = 244
    elif chinAsymm > 0.5:
        chinAsymm_cat = 'Вправо'
        chinAsymm_value = 266 
        
    chinAsymm_key = 7
    return chinAsymm, chinAsymm_cat, chinAsymm_key, chinAsymm_value, axis, angle


#Выраженность скул
def cheek_bone(pts_shape):
    
    dY = pts_shape[14][1] - shape[27][1]
    dX = pts_shape[14][0] - shape[27][0]
    axis = np.degrees(np.arctan2(dX, dY)) 

    dY = pts_shape[9][1] - pts_shape[7][1]
    dX = pts_shape[9][0] - pts_shape[7][0]
    angle1 = np.degrees(np.arctan2(dX, dY))

    dY = pts_shape[10][1] - pts_shape[8][1]
    dX = pts_shape[10][0] - pts_shape[8][0]
    angle2 = np.degrees(np.arctan2(dX, dY))
    
    cheekBone = angle2 - angle1 - axis*2
    
    if cheekBone <= 2:
        cheekBone_cat = 'Не выражены'
        cheekBone_value = 91
    elif 2 < cheekBone <= 6:
        cheekBone_cat = 'Cредне выражены'
        cheekBone_value = 174
    elif cheekBone > 6:
        cheekBone_cat = 'Выражены'
        cheekBone_value = 94 
        
    cheekBone_key = 25
    return cheekBone, cheekBone_cat, cheekBone_key, cheekBone_value, axis, angle1, angle2


#Высота носа
def nose_height(pts_shape):
    
    noseBotPoint = ((pts_shape[17][0] + pts_shape[18][0]) // 2, (pts_shape[17][1] + pts_shape[18][1]) // 2)
    noseTopPoint = shape[27]
    noseHeight = dist.euclidean(noseBotPoint, noseTopPoint)
    headWidth =  dist.euclidean(pts_shape[7], pts_shape[8])
    
    noseHeight = noseHeight / headWidth * 10
    if noseHeight <= 3.41:
        noseHeight_cat = 'Низкий'
        noseHeight_value = 63
    elif 3.41 < noseHeight <= 3.65:
        noseHeight_cat = 'Cредний'
        noseHeight_value = 65
    elif noseHeight > 3.65:
        noseHeight_cat = 'Высокий'
        noseHeight_value = 67 
        
    noseHeight_key = 17
    return noseHeight, noseHeight_cat, noseHeight_key, noseHeight_value, noseBotPoint, noseTopPoint


#Полнота губ
def lips_width(pts_shape): 
         #Высота губ
    topLipDist =  dist.euclidean(shape[50], shape[61]) + dist.euclidean(shape[52], shape[63]) + \
                  (shape[62][1] - shape[49][1]) + (shape[62][1] - shape[53][1])
    
    
    botLipDist =  dist.euclidean(pts_shape[16], shape[66]) * 3

    headWidth = dist.euclidean(pts_shape[7], pts_shape[8])
    headHight = dist.euclidean(pts_shape[0], pts_shape[14])
    
    TopLipWidth = topLipDist / headWidth
    BottomlipWidth = botLipDist / (headWidth + headHight) * 10
    
    if TopLipWidth <= 0.15:
        topLipWidth_cat = 'Тонкая'
        topLip_value = 37
    elif 0.15 < TopLipWidth <= 0.23:
        topLipWidth_cat = 'Средняя'
        topLip_value = 39
    elif 0.23 < TopLipWidth:
        topLipWidth_cat = 'Полная'
        topLip_value = 41

    if BottomlipWidth <= 0.70:
        botLipWidth_cat = 'Тонкая'
        botLip_value = 32
    elif 0.70 < BottomlipWidth <= 0.95:
        botLipWidth_cat = 'Средняя' 
        botLip_value = 34 
    elif 0.95 < BottomlipWidth:
        botLipWidth_cat = 'Полная' 
        botLip_value = 36
    
    topLipFulness_key = 10
    botLipFulness_key = 9

    return TopLipWidth, BottomlipWidth, topLipWidth_cat, botLipWidth_cat, botLipFulness_key, topLipFulness_key, botLip_value, topLip_value

#Ширина носа
def nose_width(pts_shape): 
    noseWidth = dist.euclidean(shape[19], shape[20])
    headWidth = dist.euclidean(pts_shape[7], pts_shape[8])
    
    noseWidth = noseWidth / headWidth
    
    if noseWidth <= 0:
        noseWidth_cat = 'Узкий'
        noseWidth_value = 68

    elif 1 < noseWidth <= 1:
        noseWidth_cat = 'Средний'
        noseWidth_value = 70

    elif noseWidth > 0:
        noseWidth_cat = 'Широкий'
        noseWidth_value = 72
        
    noseWidth_key = 18
    return noseWidth, noseWidth_cat, noseWidth_key, noseWidth_value, noseWidth, noseWidth


#Асимметрия брови
def brow_asymm(pts_shape): 
    # extract the left and right eye (x, y)-coordinates
    leftEyePts = shape[42:46]
    rightEyePts = shape[36:40]

    # compute the center of mass for each eye
    leftEyeCenter = leftEyePts.mean(axis=0).astype("int")
    rightEyeCenter = rightEyePts.mean(axis=0).astype("int")

    # compute the angle between the eye centroids
    dY = rightEyeCenter[1] - leftEyeCenter[1]
    dX = rightEyeCenter[0] - leftEyeCenter[0]
    axis = np.degrees(np.arctan2(dX, dY)) + 90
    
    dY = pts_shape[2][1] - pts_shape[4][1]
    dX = pts_shape[2][0] - pts_shape[4][0]
    angle = np.degrees(np.arctan2(dX, dY)) + 90

    browAsymm = angle - axis
    
    if browAsymm <= -0.8:
        browAsymm_cat = 'Правая вверх'
        browAsymm_value = 68

    elif -0.8 < browAsymm <= 0.8:
        browAsymm_cat = 'Нет асимметрии'
        browAsymm_value = 70

    elif browAsymm > 0.8:
        browAsymm_cat = 'Левая вверх'
        browAsymm_value = 72
        
    browAsymm_key = 48
    return browAsymm, browAsymm_cat, browAsymm_key, browAsymm_value, axis, browAsymm

def correct_lips(): 
             #Заменить перемешанные пиксели
    pixel_between_lips1 = (abs(shape[61][1] - shape[67][1]))/2
    if shape[61][1] > shape[67][1]:
        shape[61][1] = shape[61][1] - pixel_between_lips1
        shape[67][1] =  shape[67][1] + pixel_between_lips1
    elif shape[61][1] < shape[67][1]:
        shape[61][1] = shape[61][1] + pixel_between_lips1
        shape[67][1] =  shape[67][1] - pixel_between_lips1

    pixel_between_lips2 = (abs(shape[62][1] - shape[66][1]))/2
    if shape[62][1] > shape[66][1]:
        shape[62][1] = shape[62][1] - pixel_between_lips2
        shape[66][1] =  shape[66][1] + pixel_between_lips2
    elif shape[62][1] < shape[66][1]:
        shape[62][1] = shape[62][1] + pixel_between_lips2
        shape[66][1] =  shape[66][1] - pixel_between_lips2

    pixel_between_lips3 = (abs(shape[63][1] - shape[65][1]))/2
    if shape[63][1] > shape[65][1]:
        shape[63][1] = shape[63][1] - pixel_between_lips3
        shape[65][1] =  shape[65][1] + pixel_between_lips3
    elif shape[63][1] < shape[65][1]:
        shape[63][1] = shape[63][1] + pixel_between_lips3
        shape[65][1] =  shape[65][1] - pixel_between_lips3
    return shape


#Размер век
def veki_size(pts_shape):

    eyeRight = ((shape[37][0] + shape[38][0]) // 2, (shape[37][1] + shape[38][1]) // 2)
   
    eyeLeft = ((shape[43][0] + shape[44][0]) // 2, (shape[43][1] + shape[44][1]) // 2)
   
    vekoRight = dist.euclidean(eyeRight, pts_shape[5])
    vekoLeft = dist.euclidean(eyeLeft, pts_shape[6])
    
    vekiDist1 = dist.euclidean(eyeRight, pts_shape[5]) + dist.euclidean(eyeLeft, pts_shape[6])
    vekiDist2 = dist.euclidean(shape[37], pts_shape[5]) + dist.euclidean(shape[43], pts_shape[6])
    vekiDist3 = dist.euclidean(shape[38], pts_shape[5]) + dist.euclidean(shape[44], pts_shape[6])
    
    vekiDist = min(vekiDist1, vekiDist2, vekiDist3)
    
    headWidth =  dist.euclidean(pts_shape[7], pts_shape[8])
    
    vekiSize = vekiDist / headWidth * 10
    
    if vekiSize <= 0.65:
        vekiSize_cat = 'Гипертонус'
        vekiSize_value = 201   
    elif 0.65 < vekiSize <= 1.2:
        vekiSize_cat = 'Выраженные'
        vekiSize_value = 203
    elif vekiSize > 1.2:
        vekiSize_cat = 'Опущенные'
        vekiSize_value = 202 
        
    vekiSize_key = 55
    return vekiSize, vekiSize_cat, vekiSize_key, vekiSize_value, eyeRight, eyeLeft


#Размер век
def veki_asymm(pts_shape):

    eyeRight = ((shape[37][0] + shape[38][0]) // 2, (shape[37][1] + shape[38][1]) // 2)
   
    eyeLeft = ((shape[43][0] + shape[44][0]) // 2, (shape[43][1] + shape[44][1]) // 2)
   
    vekoRight = dist.euclidean(eyeRight, pts_shape[5])
    vekoLeft = dist.euclidean(eyeLeft, pts_shape[6])

    headWidth =  dist.euclidean(pts_shape[7], pts_shape[8])
    
    vekiAsymm = vekoRight / vekoLeft 
    
    if vekiAsymm <= 0.7:
        vekiAsymm_cat = 'Атрофия справа'
        vekiAsymm_value = 201  
    elif 0.7 < vekiAsymm <= 1.3:
        vekiAsymm_cat = 'Нет асимметрии'
        vekiAsymm_value = 203
    elif vekiAsymm > 1.4:
        vekiAsymm_cat = 'Атрофия слева'
        vekiAsymm_value = 202 
        
    vekiAsymm_key = 55
    return vekiAsymm, vekiAsymm_cat, vekiAsymm_key, vekiAsymm_value, eyeRight, eyeLeft


def print_func(pts_shape):

    dict = {}


    headForm, head_form_cat, head_form_key, head_form_value, head_height, head_width = head_form(pts_shape)
    print('Форма головы', '\t', head_form_cat, '\t\t', headForm)
    dict[head_form_key] = head_form_value

    browAngle, browAngle_cat, browAngle_key, browAngle_value, angleLeft, angleRight = brow_angle(pts_shape)
    print('Наклон бровей', '\t', browAngle_cat, '\t\t', browAngle)
    dict[browAngle_key] = browAngle_value

    EyeSize, EyeSize_cat, eye_size_key, eye_size_value, MEheight, MEwidth = eye_size(pts_shape)
    print('Размер глаз', '\t', EyeSize_cat, '\t\t', EyeSize)
    dict[eye_size_key] = eye_size_value

    eyeWidth, eyeWidth_cat, eyeWidth_key, eyeWidth_value, pointRight, pointLeft = eye_width(pts_shape)
    print('Шир. посадки глаз', '\t', eyeWidth_cat, '\t\t', eyeWidth)
    dict[eyeWidth_key] = eyeWidth_value

    mouthSize, mouthSize_cat, mouthSize_key, mouthSize_value, headWidth, mouthDist = mouth_size(pts_shape)
    print('Размер рта', '\t', mouthSize_cat, '\t\t', mouthSize)
    dict[mouthSize_key] = mouthSize_value

    jawWidth, jawWidth_cat, jawWidth_key, jawWidth_value, jawWidth, headWidth = jaw_width(pts_shape)
    print('Ширина челюсти', '\t', jawWidth_cat, '\t\t', jawWidth)
    dict[jawWidth_key] = jawWidth_value

    chinWidth, chinWidth_cat, chinWidth_key, chinWidth_value, angle, angle2 = chin_width(pts_shape)
    print('Ширина подбородка', '\t', chinWidth_cat, '\t\t', chinWidth)
    dict[chinWidth_key] = chinWidth_value

    chinWeight, chinWeight_cat, chinWeight_key, chinWeight_value, chinWeight, headHeight = chin_weight(pts_shape)
    print('Тяжесть подбородка', '\t', chinWeight_cat, '\t\t', chinWeight)
    dict[chinWeight_key] = chinWeight_value

    nostrilsWidth, nostrilsWidth_cat, nostrilsWidth_key, nostrilsWidth_value, nostrilsWidth, nostrilsWidth = nostrils_width(pts_shape)
    print('Ширина ноздрей', '\t', nostrilsWidth_cat, '\t\t', nostrilsWidth)
    dict[nostrilsWidth_key] = nostrilsWidth_value

    cheekBone, cheekBone_cat, cheekBone_key, cheekBone_value, axis, angle1, angle2 = cheek_bone(pts_shape)
    print('Выраженность скул', '\t', cheekBone_cat, '\t\t', cheekBone)
    dict[cheekBone_key] = cheekBone_value

    noseHeight, noseHeight_cat, noseHeight_key, noseHeight_value, noseBotPoint, noseTopPoint = nose_height(pts_shape)
    print('Высота носа', '\t', noseHeight_cat, '\t\t', noseHeight)
    dict[noseHeight_key] = noseHeight_value

    TopLipWidth, BottomlipWidth, topLipWidth_cat, botLipWidth_cat, botLipFulness_key, topLipFulness_key, botLip_value, topLip_value = lips_width(pts_shape)
    print('Верхняя губа', '\t', topLipWidth_cat, '\t\t', TopLipWidth)
    print('Нижняя губа', '\t', botLipWidth_cat, '\t\t', BottomlipWidth)
    dict[topLipFulness_key] = topLip_value
    dict[botLipFulness_key] = botLip_value

    jelvakAsymm, jelvakAsymm_cat, jelvakAsymm_key, jelvakAsymm_value, leftJelvak, rightJelvak = jelvak_asymetry(pts_shape)
    print('Асимметрия желваков', '\t', jelvakAsymm_cat, '\t\t', jelvakAsymm)
    dict[jelvakAsymm_key] = jelvakAsymm_value

    jawAsymm, jawAsymm_cat, jawAsymm_key, jawAsymm_value, axis, angle = jaw_asymmetry(pts_shape)
    print('Асимметрия челюсти', '\t', jawAsymm_cat, '\t\t', jawAsymm)
    dict[jawAsymm_key] = jawAsymm_value

    browAsymm, browAsymm_cat, browAsymm_key, browAsymm_value, axis, browAsymm = brow_asymm(pts_shape)
    print('Асимметрия брови', '\t', browAsymm_cat, '\t\t', browAsymm)
    dict[browAsymm_key] = browAsymm_value

    vekiSize, vekiSize_cat, vekiSize_key, vekiSize_value, eyeRight, eyeLeft = veki_size(pts_shape)
    print('Верхнее веко', '\t', vekiSize_cat, '\t\t', vekiSize)
    dict[vekiSize_key] = vekiSize_value

    vekiAsymm, vekiAsymm_cat, vekiAsymm_key, vekiAsymm_value, eyeRight, eyeLeft = veki_asymm(pts_shape)
    print('Асимметрия век', '\t', vekiAsymm_cat, '\t\t', vekiAsymm)
    dict[vekiAsymm_key] = vekiAsymm_value

    return dict


def visualize(image, pts_shape):
	for (x, y) in pts_shape:
		cv2.circle(image, (x, y), 3, (0, 0, 255), -1)
	for (x, y) in shape:
		cv2.circle(image, (x, y), 3, (0, 255, 255), -1)
	cv2.namedWindow('window', cv2.WINDOW_AUTOSIZE)
	cv2.imshow('window', image)
	cv2.waitKey(0)
	cv2.destroyAllWindows() 


def main():

    argumentParser = argparse.ArgumentParser()
    argumentParser.add_argument("--img_path", required=True, help="path to IMAGE.")
    argumentParser.add_argument("--pts_path", required=True, help="path to PTS.")
    argumentParser.add_argument("--shape_predictor", required=False, help="path to shape_predictor.dat")
    argumentParser.add_argument('--show_img', default=False, help='show image with points if "Yes"')
    arguments = vars(argumentParser.parse_args())

    detector = dlib.get_frontal_face_detector()
    
    if arguments['shape_predictor'] != None:
        predictor = dlib.shape_predictor(arguments['shape_predictor'])
    else: 
        predictor = dlib.shape_predictor('shape_predictor_68_face_landmarks.dat')

    pts_shape_path = arguments['pts_path']
    pts_shape = load(pts_shape_path)

    image_path = arguments['img_path']
    image = cv2.imread(image_path)

    faces = detector(image, 0)
    for face in faces:
        global shape
        shape = predictor(image, face)
        shape = shape_to_np(shape)

    shape = correct_lips()

    dict = print_func(pts_shape)

    keys = []
    for key in dict.keys():
        keys.append(key)
    print(keys)

    json_string = json.dumps(dict)  
    print(json_string)

    if arguments['show_img'] == 'Yes':
        visualize(image, pts_shape)

    return json_string, keys
    #return json_string, keys


if __name__ == "__main__":
    main()



### TEST ALL ANFAS
'''
def main():

    detector = dlib.get_frontal_face_detector()
    predictor = dlib.shape_predictor('shape_predictor_68_face_landmarks.dat')

    for img in os.listdir('images2/images2_canvas/'):
        print('******************************************************')
        print('******************************************************')
        print('******************************************************')
        print('---   {}   --- '.format(img))

        image_path = 'images2/images2_canvas/{}'.format(img)
        pts_shape_path = 'images2/images2_annotations/{}.pts'.format(img[:-4])

        pts_shape = load(pts_shape_path)
        image = cv2.imread(image_path)
        faces = detector(image, 0)

        for face in faces:
            global shape
            shape = predictor(image, face)
            shape = shape_to_np(shape)

        shape = correct_lips()

        print_func(pts_shape)
        visualize(image, pts_shape)
'''



### FROM LOCAL COMPUTER BY PHOTO ID
'''

def main():

    detector = dlib.get_frontal_face_detector()
    predictor = dlib.shape_predictor('shape_predictor_68_face_landmarks.dat')

    argumentParser = argparse.ArgumentParser()
    argumentParser.add_argument("--id", required='required', help="ID of the image.")
    arguments = vars(argumentParser.parse_args())

    pts_shape_path = 'images/images_annotations/{}_1_resized.pts'.format(arguments['id'])
    pts_shape = load(pts_shape_path)

    image_path = 'images/images_canvas/{}_1_resized.jpg'.format(arguments['id'])
    image = cv2.imread(image_path)
    faces = detector(image, 0)

    for face in faces:
        global shape
        shape = predictor(image, face)
        shape = shape_to_np(shape)

    shape = correct_lips()

    print_func(pts_shape)
'''