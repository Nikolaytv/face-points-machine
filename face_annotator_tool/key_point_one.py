# -*- coding: utf-8 -*-

import sys, os
from PIL import Image, ImageTk
import tkinter as tk
import math

try:
	path = sys.argv[1]
except IndexError:
	print("Usage: python key_point.py <path-to-img>")
	print("You need to access the image with at least one folder name,\
	 so that the annotation files can be placed correctly")
	sys.exit(1)

N_points = 30
point_now = 0
point_pos = [None for i in range(N_points)]
Buttons = []
keyid = 1

folder_path, img_name = os.path.split(path)

print("folder_path: ", folder_path)

last_folder_name = [part for part in folder_path.split('/') if part != ''][-1]
canvas_path = os.path.join(folder_path, '_canvas')
annotationPath = os.path.join(folder_path, '_annotations')

if not os.path.exists(canvas_path):
	os.makedirs(canvas_path)
if not os.path.exists(annotationPath):
	os.makedirs(annotationPath)    

out_name = img_name.split('.')[0]

point_now = 0
point_pos = [None for i in range(N_points)]
Buttons = []

try:
	img = Image.open(open(path, 'rb'))
except IOError:
	print("Unable to open image: " + path)
	exit(1)
	
top = tk.Tk()
top.wm_title(img_name)

resized = False
resize_multiple = 1  
size = img.size
print("original size: ", size)
img_size = list(size)


screenWidth = top.winfo_screenwidth()
screenHeight = top.winfo_screenheight()

resize_multiple = size[1]/screenHeight
print('resize multiple: ', resize_multiple)
img_size[0] = int(size[0] / resize_multiple)
img_size[1] = int(size[1] / resize_multiple)
img = img.resize(tuple(img_size), Image.ANTIALIAS)
print('new img size', img.size)


displayImage = ImageTk.PhotoImage(img)
canvas = tk.Canvas(top, width=img_size[0], height=img_size[1])
imagesprite = canvas.create_image(img_size[0]/2, img_size[1]/2, image=displayImage)    
'''	
xsb = tk.Scrollbar(top, orient="horizontal", command=canvas.xview)
ysb = tk.Scrollbar(top, orient="vertical", command=canvas.yview)
canvas.configure(yscrollcommand=ysb.set, xscrollcommand=xsb.set)
canvas.configure(scrollregion=(0,0,1000,1000))
'''

#xsb.pack()
#ysb.pack()

#canvas.pack()
#top.grid_rowconfigure(0, weight=1)
#top.grid_columnconfigure(0, weight=1)

rect = [None for i in range(N_points)]
#bbox = None


def reDraw(num):
	global rect
	global N_points
	#global bbox
	global text

	if rect[num] != None:
		canvas.delete(rect[num])
		rect[num] = None
	if point_pos[num] != None:
		pt = point_pos[num]
		rect[num] = canvas.create_rectangle(pt[0]-2, pt[1]-2, pt[0]+2, pt[1]+2, fill="red")


	#PUT NUMBER OF POINT NEXT TO THE POINT
	canvas.delete('label')
	asd = canvas.create_text(100,30, text=int(point_now)+1, tag='label')
	pointID = canvas.coords(asd, (pt[0]+10, pt[1]+10))


def clearPoint(num):
	print('clear point: ', num)
	global rect
	global bbox
	if rect[num] != None:
		canvas.delete(rect[num])
		rect[num] = None

	   

def getCoord(event):
	global point_pred
	point_pos[point_now] = (event.x, event.y)
	reDraw(point_now)


toolbox = tk.Toplevel()

def btn1():
	global point_now
	point_now = 0
#        reDraw(point_now)
btn_1 = tk.Button(toolbox, text="Head top left(1)", command=btn1)
Buttons.append(btn_1)


def btn2():
	global point_now
	point_now = 1
#        reDraw(point_now)
btn_2 = tk.Button(toolbox, text="Head bottom right(2)", command=btn2)
Buttons.append(btn_2)

def btn3():
	global point_now
	point_now = 2   
#        reDraw(point_now)
btn_3 = tk.Button(toolbox, text="left eye(3)", command=btn3)
Buttons.append(btn_3)


def btn4():
	global point_now
	point_now = 3  
#        reDraw(point_now)
btn_4 = tk.Button(toolbox, text="right eye(4)", command=btn4)
Buttons.append(btn_4)

def btn5():
	global point_now
	point_now = 4 
#        reDraw(point_now)
btn_5 = tk.Button(toolbox, text="nose(5)", command=btn5)
Buttons.append(btn_5)


def btn6():
	global point_now
	point_now = 5 
#        reDraw(point_now)
btn_6 = tk.Button(toolbox, text="left mouse(6)", command=btn6)
Buttons.append(btn_6)

def btn7():
	global point_now
	point_now = 6 
#        reDraw(point_now)
btn_7 = tk.Button(toolbox, text="right mouse(7)", command=btn7)
Buttons.append(btn_7)

def btn8():
	global point_now
	point_now = 7
#        reDraw(point_now)
btn_8 = tk.Button(toolbox, text="right mouse(8)", command=btn8)
Buttons.append(btn_8)

def btn9():
	global point_now
	point_now = 8
#        reDraw(point_now)
btn_9 = tk.Button(toolbox, text="right mouse(9)", command=btn9)
Buttons.append(btn_9)

def btn10():
	global point_now
	point_now = 9
#        reDraw(point_now)
btn_0 = tk.Button(toolbox, text="right mouse(0)", command=btn10)
Buttons.append(btn_0)

def btn11():
	global point_now
	point_now = 10 
#        reDraw(point_now)
btn_Q = tk.Button(toolbox, text="right mouse(q)", command=btn11)
Buttons.append(btn_Q)

def btn12():
	global point_now
	point_now = 11 
#        reDraw(point_now)
btn_W = tk.Button(toolbox, text="right mouse(w)", command=btn12)
Buttons.append(btn_W)

def btn13():
	global point_now
	point_now = 12
#        reDraw(point_now)
btn_E = tk.Button(toolbox, text="right mouse(e)", command=btn13)
Buttons.append(btn_E)

def btn14():
	global point_now
	point_now = 13
#        reDraw(point_now)
btn_R = tk.Button(toolbox, text="right mouse(r)", command=btn14)
Buttons.append(btn_R)

def btn15():
	global point_now
	point_now = 14
#        reDraw(point_now)
btn_T = tk.Button(toolbox, text="right mouse(t)", command=btn15)
Buttons.append(btn_T)

def btn16():
	global point_now
	point_now = 15
#        reDraw(point_now)
btn_Y = tk.Button(toolbox, text="right mouse(y)", command=btn16)
Buttons.append(btn_Y)

def btn17():
	global point_now
	point_now = 16
#        reDraw(point_now)
btn_U = tk.Button(toolbox, text="right mouse(u)", command=btn17)
Buttons.append(btn_U)

def btn18():
	global point_now
	point_now = 17
#        reDraw(point_now)
btn_I = tk.Button(toolbox, text="right mouse(i)", command=btn18)
Buttons.append(btn_I)


def btn19():
	global point_now
	point_now = 18
#        reDraw(point_now)
btn_O = tk.Button(toolbox, text="right mouse(o)", command=btn19)
Buttons.append(btn_O)

def btn20():
	global point_now
	point_now = 19
#        reDraw(point_now)
btn_P = tk.Button(toolbox, text="right mouse(p)", command=btn20)
Buttons.append(btn_P)

def btn21():
	global point_now
	point_now = 20
#        reDraw(point_now)
btn_A = tk.Button(toolbox, text="right mouse(a)", command=btn21)
Buttons.append(btn_A)

def btn22():
    global point_now
    point_now = 21
#        reDraw(point_now)
btn_S = tk.Button(toolbox, text="right mouse(a)", command=btn22)
Buttons.append(btn_S)

def btn23():
    global point_now
    point_now = 22
#        reDraw(point_now)
btn_D = tk.Button(toolbox, text="right mouse(a)", command=btn23)
Buttons.append(btn_D)

def btn24():
    global point_now
    point_now = 23
#        reDraw(point_now)
btn_F = tk.Button(toolbox, text="right mouse(a)", command=btn24)
Buttons.append(btn_F)

def btn25():
    global point_now
    point_now = 24
#        reDraw(point_now)
btn_G = tk.Button(toolbox, text="right mouse(a)", command=btn25)
Buttons.append(btn_G)

def btn26():
    global point_now
    point_now = 25
#        reDraw(point_now)
btn_H = tk.Button(toolbox, text="right mouse(a)", command=btn26)
Buttons.append(btn_H)

def btn27():
    global point_now
    point_now = 26
#        reDraw(point_now)
btn_J = tk.Button(toolbox, text="right mouse(a)", command=btn27)
Buttons.append(btn_J)

def btn28():
    global point_now
    point_now = 27
#        reDraw(point_now)
btn_K = tk.Button(toolbox, text="right mouse(a)", command=btn28)
Buttons.append(btn_K)

def btn29():
    global point_now
    point_now = 28
#        reDraw(point_now)
btn_L = tk.Button(toolbox, text="right mouse(a)", command=btn29)
Buttons.append(btn_L)

def btn30():
    global point_now
    point_now = 29
#        reDraw(point_now)
btn_v = tk.Button(toolbox, text="right mouse(a)", command=btn30)
Buttons.append(btn_v)

def btnC():
	global point_pos
	point_pos[point_now] = None
	clearPoint(point_now)
btn_C = tk.Button(toolbox, text="Clear this point(c)", command=btnC)
Buttons.append(btn_C)

def btnN():
#        global point_pos
	print('Next Image')
	#result = Image.fromarray(img)
	img.save(os.path.join(canvas_path, out_name+"_resized.jpg"))
	#scv2.imwrite(os.path.join(canvas_path, out_name+"_canvas.jpg"), img)
	#canvas.postscript(file=os.path.join(canvas_path, out_name+".ps"), colormode='color')    
	top.destroy()
btn_N = tk.Button(toolbox, text="Next Image(n)", command=btnN)
Buttons.append(btn_N)


#btn_D = tk.Button(toolbox, text="Draw Bbox(d)", command=drawBBox)
#Buttons.append(btn_D)


def key(event):
    global keyid
    print("pressed", repr(event.char))

    if event.char == 'x':
        keyid += 1
        btn = 'btn{}()'.format(str(keyid))
        print(btn)
        exec(btn)
    elif event.char == 'z':
        keyid -= 1
        btn = 'btn{}()'.format(str(keyid))
        print(btn)
        exec(btn)

    elif event.char == '1':
        keyid = 1
        btn = 'btn{}()'.format(str(keyid))
        exec(btn)
    elif event.char == '2':
        keyid = 2
        btn = 'btn{}()'.format(str(keyid))
        print(btn)
        exec(btn)
    elif event.char == '3':
        keyid = 3
        btn = 'btn{}()'.format(str(keyid))
        print(btn)
        exec(btn)
    elif event.char == '4':
        keyid = 4
        btn = 'btn{}()'.format(str(keyid))
        exec(btn)
    elif event.char == '5':
        keyid = 5
        btn = 'btn{}()'.format(str(keyid))
        exec(btn)
    elif event.char == '6':
        keyid = 6
        btn = 'btn{}()'.format(str(keyid))
        exec(btn)
    elif event.char == '7':
        keyid = 7
        btn = 'btn{}()'.format(str(keyid))
        exec(btn)
    elif event.char == '8':
        keyid = 8
        btn = 'btn{}()'.format(str(keyid))
        exec(btn)
    elif event.char == '9':
        keyid = 9
        btn = 'btn{}()'.format(str(keyid))
        exec(btn)
    elif event.char == '0':
        keyid = 10
        btn = 'btn{}()'.format(str(keyid))
        exec(btn)
    elif event.char == 'q':
        keyid = 11
        btn = 'btn{}()'.format(str(keyid))
        exec(btn)
    elif event.char == 'w':
        keyid = 12
        btn = 'btn{}()'.format(str(keyid))
        exec(btn)
    elif event.char == 'e':
        keyid = 13
        btn = 'btn{}()'.format(str(keyid))
        exec(btn)
    elif event.char == 'r':
        keyid = 14
        btn = 'btn{}()'.format(str(keyid))
        exec(btn)
    elif event.char == 't':
        keyid = 15
        btn = 'btn{}()'.format(str(keyid))
        exec(btn)
    elif event.char == 'y':
        keyid = 16
        btn = 'btn{}()'.format(str(keyid))
        exec(btn)
    elif event.char == 'u':
        keyid = 17
        btn = 'btn{}()'.format(str(keyid))
        exec(btn)
    elif event.char == 'i':
        keyid = 18
        btn = 'btn{}()'.format(str(keyid))
        exec(btn)
    elif event.char == 'o':
        keyid = 19
        btn = 'btn{}()'.format(str(keyid))
        exec(btn)
    elif event.char == 'p':
        keyid = 20
        btn = 'btn{}()'.format(str(keyid))
        exec(btn)
    elif event.char == 'a':
        keyid = 21
        btn = 'btn{}()'.format(str(keyid))
        exec(btn)
    elif event.char == 's':
        keyid = 22
        btn = 'btn{}()'.format(str(keyid))
        exec(btn)
    elif event.char == 'd':
        keyid = 23
        btn = 'btn{}()'.format(str(keyid))
        exec(btn)
    elif event.char == 'f':
        keyid = 24
        btn = 'btn{}()'.format(str(keyid))
        exec(btn)
    elif event.char == 'g':
        keyid = 25
        btn = 'btn{}()'.format(str(keyid))
        exec(btn)
    elif event.char == 'h':
        keyid = 26
        btn = 'btn{}()'.format(str(keyid))
        exec(btn)
    elif event.char == 'j':
        keyid = 27
        btn = 'btn{}()'.format(str(keyid))
        exec(btn)
    elif event.char == 'k':
        keyid = 28
        btn = 'btn{}()'.format(str(keyid))
        exec(btn)
    elif event.char == 'l':
        keyid = 29
        btn = 'btn{}()'.format(str(keyid))
        exec(btn)
    elif event.char == 'v':
        keyid = 30
        btn = 'btn{}()'.format(str(keyid))
        exec(btn)

    elif event.char == 'n':
        btnN()

for b in Buttons:
    b.pack()



canvas.bind("<Button-1>",getCoord)

canvas.bind("<Key>", key)    
canvas.pack()

canvas.focus_set()

top.geometry('%dx%d+%d+%d' % (img_size[0], img_size[1], 150, 0))

top.mainloop()

fout = open(os.path.join(annotationPath, out_name+'_resized.pts'), 'w')
fout.write("{\n")
for i in range(N_points):
	if point_pos[i] != None:

		if resized:
			fout.write(str(int(point_pos[i][0]*resize_multiple))+" "+str(int(point_pos[i][1]*resize_multiple)) + "\n")
		else:
			fout.write(str(int(point_pos[i][0]))+" "+str(int(point_pos[i][1])) + "\n")
	else:
		fout.write("-1 -1"+"\n")

fout.write("}")
fout.close()