import cv2
import numpy as np
import dlib
import argparse

def shape_to_np(shape, dtype="int"):
    # initialize the list of (x, y)-coordinates
    coords = np.zeros((68, 2), dtype=dtype)
 
    # loop over the 68 facial landmarks and convert them
    # to a 2-tuple of (x, y)-coordinates
    for i in range(0, 68):
        coords[i] = (shape.part(i).x, shape.part(i).y)
    

    # return the list of (x, y)-coordinates
    return coords

class FaceAligner:
    def __init__(self, predictor):
        # store the facial landmark predictor, desired output left
        # eye position, and desired output face width + height
        self.predictor  = predictor 
        
    def align(self, image, face):
        # convert the landmark (x, y)-coordinates to a NumPy array
        shape = self.predictor(image, face)
        shape = shape_to_np(shape)
            
        # extract the left and right eye (x, y)-coordinates
        leftEyePts = shape[42:46]
        rightEyePts = shape[36:40]
        
        # compute the center of mass for each eye
        leftEyeCenter = leftEyePts.mean(axis=0).astype("int")
        rightEyeCenter = rightEyePts.mean(axis=0).astype("int")
 
        # compute the angle between the eye centroids
        dY = rightEyeCenter[1] - leftEyeCenter[1]
        dX = rightEyeCenter[0] - leftEyeCenter[0]
        angle = np.degrees(np.arctan2(dY, dX)) - 180

        # determine the scale of the new resulting image by taking
        # the ratio of the distance between eyes in the *current*
        # image to the ratio of distance between eyes in the
        # *desired* image
        dist = np.sqrt((dX ** 2) + (dY ** 2))
        
        # compute center (x, y)-coordinates (i.e., the median point)
        # between the two eyes in the input image
        eyesCenter = ((leftEyeCenter[0] + rightEyeCenter[0]) // 2,
            (leftEyeCenter[1] + rightEyeCenter[1]) // 2)

        # grab the rotation matrix for rotating and scaling the face
        M = cv2.getRotationMatrix2D(eyesCenter, angle, scale=1)
 
        # apply the affine transformation
        output = cv2.warpAffine(image, M, (image.shape[1], image.shape[0]),
            flags=cv2.INTER_CUBIC)
 
        # return the aligned face
        return output



def main():
    argumentParser = argparse.ArgumentParser()
    argumentParser.add_argument("--image", required='required', help="Path to image.")
    arguments = vars(argumentParser.parse_args())

    image = cv2.imread(arguments['image'])
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
           
    predictor = dlib.shape_predictor('shape_predictor_68_face_landmarks.dat')
    fa = FaceAligner(predictor)

    detector = dlib.get_frontal_face_detector()
    faces = detector(gray, 2)

    # loop over the face detections
    for face in faces:
        

        # extract the ROI of the *original* face, then align the face
        # using facial landmarks
        faceAligned = fa.align(gray, face)
     
        # display the output images
        cv2.imshow("Original", gray)
        cv2.imshow("Aligned", faceAligned)
        cv2.waitKey(0)
    cv2.destroyAllWindows()



if __name__ == "__main__":
    main()