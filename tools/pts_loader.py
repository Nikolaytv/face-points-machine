import cv2

def load(path):
    """takes as input the path to a .pts and returns a list of 
    tuples of floats containing the points in in the form:
    [(x_0, y_0, z_0),
     (x_1, y_1, z_1),
     ...
     (x_n, y_n, z_n)]"""
    with open(path) as f:
        rows = [rows.strip() for rows in f]
    
    """Use the curly braces to find the start and end of the point data""" 
    head = rows.index('{') + 1
    tail = rows.index('}')

    """Select the point data split into coordinates"""
    raw_points = rows[head:tail]
    coords_set = [point.split() for point in raw_points]

    """Convert entries from lists of strings to tuples of floats"""
    points = [tuple([round(float(point)) for point in coords]) for coords in coords_set]
    return points


q = load('images_annotations/8_resized.pts')
print(q)



image = cv2.imread('images_canvas/8_resized.jpg')
for (x, y) in q:
  cv2.circle(image, (x, y), 1, (0, 0, 255), -1)

cv2.imshow('window', image)
cv2.waitKey(0)
