import cv2
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg

def load(path):
    """takes as input the path to a .pts and returns a list of 
    tuples of floats containing the points in in the form:
    [(x_0, y_0, z_0),
     (x_1, y_1, z_1),
     ...
     (x_n, y_n, z_n)]"""
    with open(path) as f:
        rows = [rows.strip() for rows in f]
    
    """Use the curly braces to find the start and end of the point data""" 
    head = rows.index('{') + 1
    tail = rows.index('}')

    """Select the point data split into coordinates"""
    raw_points = rows[head:tail]
    coords_set = [point.split() for point in raw_points]

    """Convert entries from lists of strings to tuples of floats"""
    points = [tuple([round(float(point)) for point in coords]) for coords in coords_set]
    return points

def align(shape, image):
    # convert the landmark (x, y)-coordinates to a NumPy array
    shape = shape 

    shape1 = shape[0]
    shape2 = shape[1]

    # compute the angle between the eye centroids
    dY = shape1[1] - shape2[1]
    dX = shape1[0] - shape2[0]
    angle = np.degrees(np.arctan2(dY, dX)) - 90

    # determine the scale of the new resulting image by taking
    # the ratio of the distance between eyes in the *current*
    # image to the ratio of distance between eyes in the
    # *desired* image
    dist = np.sqrt((dX ** 2) + (dY ** 2))

    # compute center (x, y)-coordinates (i.e., the median point)
    # between the two eyes in the input image
    headCenter = ((shape1[0] + shape2[0]) // 2,
        (shape1[1] + shape2[1]) // 2)

    # grab the rotation matrix for rotating and scaling the face
    M = cv2.getRotationMatrix2D(headCenter, angle, scale=1)

    output = cv2.warpAffine(image, M, (image.shape[1], image.shape[0]),
        flags=cv2.INTER_CUBIC)

    # return the aligned face
    return output

shape = load('images_annotations/1_resized.pts')
image = cv2.imread('images_canvas/1_resized.jpg')

# image = cv2.imread('Prototype.0.0.1/images/images/Dugas/Small/5.jpg')
faceAligned = align(shape, image)
print(faceAligned)

cv2.imshow('aligned', faceAligned)
cv2.waitKey(0)

